# .Net面试题


## 一、C#：

1. 简述 private、 protected、 public、 internal 修饰符的访问权限。

```
答 . private :   私有成员, 在类的内部才可以访问。

     protected : 保护成员，该类内部和继承类中可以访问。

     public :    公共成员，完全公开，没有访问限制。

     internal:   在同一命名空间内可以访问。
```
 

2. 列举ASP.NET 页面之间传递值的几种方式。

```
答. 1.使用QueryString,  如....?id=1; response. Redirect()....

    2.使用Session变量

    3.使用Server.Transfer

    4.使用Application

    5.使用Cache

    6使用HttpContext的Item属性

    7.使用文件

    8.使用数据库

    9.使用Cookie
```
 

3. 一列数的规则如下: 1、1、2、3、5、8、13、21、34......  求第30位数是多少，用递归算法实现。

```
答：public class MainClass

    {

        public static void Main()  

        {

            Console.WriteLine(Foo(30));

        }

        public static int Foo(int i)

        {

            if (i <= 0)

                return 0;

            else if(i > 0 && i <= 2)

                return 1;

            else return Foo(i -1) + Foo(i - 2);

        }

    }
```
 

4.C#中的委托是什么？事件是不是一种委托？

```
答：     

 委托可以把一个方法作为参数代入另一个方法。

 委托可以理解为指向一个函数的引用。

 是，是一种特殊的委托
```
 

5. override与重载的区别

```
答：

 override 与重载的区别。重载是方法的名称相同。参数或参数类型不同，进行多次重载以适应不同的需要

 Override 是进行基类中函数的重写。实现多态。
```
 

6.如果在一个B/S结构的系统中需要传递变量值，但是又不能使用Session、Cookie、Application，您有几种方法进行处理？

```
答：

同第2题
```
 

7. 请编程遍历页面上所有TextBox控件并给它赋值为string.Empty？

```
答：

foreach (System.Windows.Forms.Control control in this.Controls)

{

    if (control is System.Windows.Forms.TextBox)

    {

        System.Windows.Forms.TextBox tb = (System.Windows.Forms.TextBox)control ;

        tb.Text = String.Empty ;

    }

}
```
 

8. 请编程实现一个冒泡排序算法？

```
答：

int [] array = new int [*] ;

 int temp = 0 ;

 for (int i = 0 ; i < array.Length - 1 ; i++)

 {

  for (int j = i + 1 ; j < array.Length ; j++)

  {

   if (array[j] < array[i])

   {

    temp = array[i] ;

    array[i] = array[j] ;

    array[j] = temp ;

   }

  }

 }
```
 

9. 描述一下C#中索引器的实现过程，是否只能根据数字进行索引？(索引器是对属性的封装,具体查看msdn)

```
答：不是。可以用任意类型。
```
 

10. 求以下表达式的值，写出您想到的一种或几种实现方法： 1-2+3-4+……+m

```
答：

int Num = this.TextBox1.Text.ToString() ;

int Sum = 0 ;

for (int i = 0 ; i < Num + 1 ; i++)

{

    if((i%2) == 1)

    {

        Sum += i ;

    }

    else

    {

        Sum = Sum  - I ;

    }

}

System.Console.WriteLine(Sum.ToString());

System.Console.ReadLine() ;
```
 

11. 用.net做B/S结构的系统，您是用几层结构来开发，每一层之间的关系以及为什么要这样分层？

```
答：使用MVC模式分层

一般为3层

        数据访问层，业务层，表示层。

 数据访问层对数据库进行增删查改。

 业务层一般分为二层，业务表观层实现与表示层的沟通，业务规则层实现用户密码的安全等。

 表示层为了与用户交互例如用户添加表单。

优点：  分工明确，条理清晰，易于调试，而且具有可扩展性。

缺点：  增加成本。
```
 

12. 在下面的例子里
```
using System;

class A

{

    public A()

    {

        PrintFields();

    }

    public virtual void PrintFields(){}

}

class B:A

{

    int x=1;

    int y;

    public B()

    {

        y=-1;

    }

    public override void PrintFields()

    {

        Console.WriteLine("x={0},y={1}",x,y);

    }
```

当使用new B()创建B的实例时，产生什么输出？

```
答：X=1,Y=0;x= 1 y = -1
```
 

13. 什么叫应用程序域？

```
答：应用程序域可以理解为一种轻量级进程。起到安全的作用。占用资源小。
```
 

14. CTS、CLS、CLR分别作何解释？

```
答：CTS：通用语言系统。CLS：通用语言规范。CLR：公共语言运行库。
```
 

15. 什么是装箱和拆箱？

```
装箱：将值类型转换为 object 类型或由此值类型实现的任何接口类型。
拆箱：将object类型或接口类型转换为实际的值类型。
```
 

16. 什么是受管制(托管)的代码？

```
答：

       托管代码是运行.NET 公共语言运行时CLR的代码

        unsafe：非托管代码。不经过CLR运行。程序员自行分配和释放内存空间
```
 

17. 什么是强命名程序集？

```
答：程序集需要经过加密签名,强命名程序集可以部署到全局程序集缓存中,成为公共程序集
```
 

18. net中读写数据库需要用到那些类？他们的作用？

```
答：DataSet:数据集。

    DataCommand:执行语句命令。

    DataAdapter:数据的集合，用语填充。

    DataReader:数据只读器
```
 

19. ASP.net的身份验证方式有哪些？分别是什么原理？

```
答：

    Windwos(默认)用IIS控制

    From(窗体)用帐户

    Passport(密钥)
```
 

20. 什么是Code-Behind技术？

```
答：代码后置。
```
 

21. 在.net中，配件的意思是？

```
答：程序集。（中间语言，源数据，资源，装配清单）
```
 

22. 常用的调用WebService的方法有哪些？

```
答：1.使用WSDL.exe命令行工具。

    2.使用VS.NET中的Add Web Reference菜单选项
```
 

23. .net Remoting 的工作原理是什么？

```
答：服务器端向客户端发送一个进程编号，一个程序域编号，以确定对象的位置。
```
 

24. 在C＃中，string str = null 与 string str = “” 请尽量使用文字或图象说明其中的区别。

```
答：string str = null 是不给他分配内存空间,而string str = "" 给它分配长度为空字符串的内存空间。
```
 

25. 请详述在C#中类(class)与结构(struct)的异同？

```
答：class可以被实例化,属于引用类型,class可以实现接口和单继承其他类,还可以作为基类型,是分配在内存的堆上的

struct属于值类型,不能作为基类型,但是可以实现接口,是分配在内存的栈上的.
```
 

26. 根据委托(delegate)的知识，请完成以下用户控件中代码片段的填写：

```
namespace test

{

public delegate void OnDBOperate();

public class UserControlBase : System.Windows.Forms.UserControl

{

 public event OnDBOperate   OnNew

 

privatevoidtoolBar_ButtonClick(objectsender,System.Windows.Forms.ToolBarButtonClickEventArgs e)

{

if(e.Button.Equals(BtnNew))

{

//请在以下补齐代码用来调用OnDBOperate委托签名的OnNew事件。

}

}

}
```

```
答：if( OnNew != null )

    OnNew( this, e );
```
 

27. 分析以下代码，完成填空

```
string strTmp = "abcdefg某某某";

int i= System.Text.Encoding.Default.GetBytes(strTmp).Length;

int j= strTmp.Length;
```
以上代码执行完后，i= j=

```
答：i=13,j=10
```
 

28. SQLSERVER服务器中，给定表 table1 中有两个字段 ID、LastUpdateDate，ID表示更新的事务号， LastUpdateDate表示更新时的服务器时间，请使用一句SQL语句获得最后更新的事务号

```
答：Select ID FROM table1 Where LastUpdateDate = (Select MAX(LastUpdateDate) FROM table1)
```
 

29. 根据线程安全的相关知识，分析以下代码，当调用test方法时i>10时是否会引起死锁?并简要说明理由。

```
public void test(int i)

{

   lock(this)

 {

   if (i>10)

   {

     i--;

     test(i);

   }

 }

}
```

```
答：不会发生死锁，（但有一点int是按值传递的，所以每次改变的都只是一个副本，因此不会出现死锁。但如果把int换做一个object，那么死锁会发生）
```
 

30. 简要谈一下您对微软.NET 构架下remoting和webservice两项技术的理解以及实际中的应用。

```
答：webservice主要是可利用HTTP，穿透防火墙。而Remoting可以利用TCP/IP，二进制传送提高效率。
```
 

31. 公司要求开发一个继承System.Windows.Forms.ListView类的组件，要求达到以下的特殊功能：点击ListView各列列头时，能按照点击列的每行值进行重排视图中的所有行 (排序的方式如DataGrid相似)。根据您的知识，请简要谈一下您的思路

```
答：根据点击的列头,包该列的ID取出,按照该ID排序后,在给绑定到ListView中。
```
 

32.给定以下XML文件，完成算法流程图。

```
<FileSystem>

< DriverC >

<Dir DirName=”MSDOS622”>

<File FileName =” Command.com” ></File>

</Dir>

<File FileName =”MSDOS.SYS” ></File>

<File FileName =” IO.SYS” ></File>

</DriverC>

</FileSystem>
```
请画出遍历所有文件名（FileName）的流程图(请使用递归算法)。

```
答：

void FindFile( Directory d )

{

   FileOrFolders = d.GetFileOrFolders();

   foreach( FileOrFolder fof in FileOrFolders )

   {

     if( fof is File )

     You Found a file;

     else if ( fof is Directory )

     FindFile( fof );

    }

}
```
 

33. 写出一条Sql语句：取出表A中第31到第40记录（SQLServer,以自动增长的ID作为主键,注意：ID可能不是连续的。

```
答：解1:  select top 10 * from A where id not in (select top 30 id from A)

    解2:  select top 10 * from A where id > (select max(id) from (select top 30 id from A )as A)
```
 

34. 面向对象的语言具有________性、_________性、________性

```
答：封装、继承、多态。
```
 

35. 能用foreach遍历访问的对象需要实现 ________________接口或声明________________方法的类型。

```
答：IEnumerable 、 GetEnumerator。
```
 

36. GC是什么? 为什么要有GC?

```
答：GC是垃圾收集器。程序员不用担心内存管理，因为垃圾收集器会自动进行管理。要请求垃圾收集，可以调用下面的方法之一：

  System.gc()

  Runtime.getRuntime().gc()
```
 

37. String s = new String("xyz");创建了几个String Object?

```
答：两个对象，一个是“xyx”,一个是指向“xyx”的引用对象s。
```
 

38. abstract class和interface有什么区别?

```
答：

声明方法的存在而不去实现它的类被叫做抽象类（abstract class），它用于要创建一个体现某些基本行为的类，并为该类声明方法，但不能在该类中实现该类的情况。不能创建abstract 类的实例。然而可以创建一个变量，其类型是一个抽象类，并让它指向具体子类的一个实例。不能有抽象构造函数或抽象静态方法。Abstract 类的子类为它们父类中的所有抽象方法提供实现，否则它们也是抽象类为。取而代之，在子类中实现该方法。知道其行为的其它类可以在类中实现这些方法。

接口（interface）是抽象类的变体。在接口中，所有方法都是抽象的。多继承性可通过实现这样的接口而获得。接口中的所有方法都是抽象的，没有一个有程序体。接口只可以定义static final成员变量。接口的实现与子类相似，除了该实现类不能从接口定义中继承行为。当类实现特殊接口时，它定义（即将程序体给予）所有这种接口的方法。然后，它可以在实现了该接口的类的任何对象上调用接口的方法。由于有抽象类，它允许使用接口名作为引用变量的类型。通常的动态联编将生效。引用可以转换到接口类型或从接口类型转换，instanceof 运算符可以用来决定某对象的类是否实现了接口。
```
 

39. 启动一个线程是用run()还是start()?

```
答：启动一个线程是调用start()方法，使线程所代表的虚拟处理机处于可运行状态，这意味着它可以由JVM调度并执行。这并不意味着线程就会立即运行。run()方法可以产生必须退出的标志来停止一个线程。
```
 

40. 接口是否可继承接口? 抽象类是否可实现(implements)接口? 抽象类是否可继承实体类(concrete class)?

```
答：接口可以继承接口。

抽象类可以实现(implements)接口，抽象类是否可继承实体类，但前提是实体类必须有明确的构造函数。
```
 

41. 构造器(构造函数)Constructor是否可被override?

```
答：构造器Constructor不能被继承，因此不能重写Overriding，但可以被重载Overloading。
```
 

42. 是否可以继承String类?

```
答：String类是密封类故不可以继承。
```
 

43. try {}里有一个return语句，那么紧跟在这个try后的finally {}里的code会不会被执行，什么时候被执行，在return前还是后?

```
答：会执行，在return前执行。
```
 

44. 两个对象值相同(x.equals(y) == true)，但却可有不同的hash code，这句话对不对?

```
答：不对，有相同的hash code。
```


45. swtich是否能作用在byte上，是否能作用在long上，是否能作用在String上?

```
答：switch（expr1）中，expr1是一个整数表达式。因此传递给 switch 和 case 语句的参数应该是 int、 short、 char 或者 byte。long,string 都不能作用于swtich。
```
 

47. 当一个线程进入一个对象的一个synchronized方法后，其它线程是否可进入此对象的其它方法?

```
不能，一个对象的一个synchronized方法只能由一个线程访问。
```
 

48. abstract的method是否可同时是static,是否可同时是native，是否可同时是synchronized?

```
答：都不能。
```
 

49. List, Set, Map是否继承自Collection接口?

```
答：List、Set是，Map不是
``` 

50. Set里的元素是不能重复的，那么用什么方法来区分重复与否呢? 是用==还是equals()? 它们有何区别?

```
答：Set里的元素是不能重复的，那么用iterator()方法来区分重复与否。equals()是判读两个Set是否相等。

equals()和==方法决定引用值是否指向同一对象equals()在类中被覆盖，为的是当两个分离的对象的内容和类型相配的话，返回真值。
```
 

51. 数组有没有length()这个方法? String有没有length()这个方法？

```
答：数组没有length()这个方法，有length的属性。String没有length()这个方法，有length属性。
```
 

52. `sleep()` 和 `wait()` 有什么区别?

```
答：sleep()方法是使线程停止一段时间的方法。在sleep 时间间隔期满后，线程不一定立即恢复执行。这是因为在那个时刻，其它线程可能正在运行而且没有被调度为放弃执行，除非(a)“醒来”的线程具有更高的优先级

(b)正在运行的线程因为其它原因而阻塞。

wait()是线程交互时，如果线程对一个同步对象x 发出一个wait()调用，该线程会暂停执行，被调对象进入等待状态，直到被唤醒或等待时间到。
```
 

53. `short s1 = 1; s1 = s1 + 1;`有什么错? `short s1 = 1; s1 += 1;`有什么错?

```
答：short s1 = 1; s1 = s1 + 1;有错，s1是short型，s1+1是int型,不能显式转化为short型。可修改为s1 =(short)(s1 + 1) 。short s1 = 1; s1 += 1正确。
```
 

54. 谈谈`final`, `finally`, `finalize`的区别。

```
答：

final—修饰符（关键字）如果一个类被声明为final，意味着它不能再派生出新的子类，不能作为父类被继承。因此     一个类不能既被声明为 abstract的，又被声明为final的。将变量或方法声明为final，可以保证它们在使用中     不被改变。被声明为final的变量必须在声明时给定初值，而在以后的引用中只能读取，不可修改。被声明为     final的方法也同样只能使用，不能重载

finally—再异常处理时提供 finally 块来执行任何清除操作。如果抛出一个异常，那么相匹配的 catch 子句就会     执行，然后控制就会进入 finally 块（如果有的话）。

finalize—方法名。Java 技术允许使用 finalize() 方法在垃圾收集器将对象从内存中清除出去之前做必要的清理     工作。这个方法是由垃圾收集器在确定这个对象没有被引用时对这个对象调用的。它是在 Object 类中定义的     ，因此所有的类都继承了它。子类覆盖 finalize() 方法以整理系统资源或者执行其他清理工作。finalize()      方法是在垃圾收集器删除对象之前对这个对象调用的。
```
 

55. 如何处理几十万条并发数据？

```
答：用存储过程或事务。取得最大标识的时候同时更新..注意主键不是自增量方式这种方法并发的时候是不会有重复主键的..取得最大标识要有一个存储过程来获取.
```
 

56. Session有什么重大BUG，微软提出了什么方法加以解决？

```
答：是iis中由于有进程回收机制，系统繁忙的话Session会丢失，可以用Sate   server或SQL   Server数据库的方式存储Session不过这种方式比较慢，而且无法捕获Session的END事件。
```
 

57. 进程和线程的区别？

```
答：进程是系统进行资源分配和调度的单位；线程是CPU调度和分派的单位，一个进程可以有多个线程，这些线程共享这个进程的资源。
```
 

58. 堆和栈的区别？

```
答：

    栈：由编译器自动分配、释放。在函数体中定义的变量通常在栈上。

    堆：一般由程序员分配释放。用new、malloc等分配内存函数分配得到的就是在堆上。
```
 

59. 成员变量和成员函数前加static的作用？

```
答：它们被称为常成员变量和常成员函数，又称为类成员变量和类成员函数。分别用来反映类的状态。比如类成员变量可以用来统计类实例的数量，类成员函数负责这种统计的动作。
```
 

60. ASP。NET与ASP相比，主要有哪些进步？

```
答：asp解释形，aspx编译型，性能提高，有利于保护源码。
```
 

61. 产生一个int数组，长度为100，并向其中随机插入1-100，并且不能重复。

```
int[] intArr=new int[100];

ArrayList myList=new ArrayList();

Random rnd=new Random();

while(myList.Count<100)

{

 int num=rnd.Next(1,101);

 if(!myList.Contains(num))

 myList.Add(num);

}

  for(int i=0;i<100;i++)

   intArr[i]=(int)myList[i];
```
 

62. 请说明在.net中常用的几种页面间传递参数的方法，并说出他们的优缺点。

```
答：session(viewstate) 简单，但易丢失

    application        全局

    cookie             简单，但可能不支持，可能被伪造

    input ttype="hidden"     简单，可能被伪造

    url参数                  简单，显示于地址栏，长度有限

    数据库                   稳定，安全，但性能相对弱
```
 

63. 请指出GAC的含义？

```
答：全局程序集缓存。
```
 

64. 向服务器发送请求有几种方式？

```
答：get,post。get一般为链接方式，post一般为按钮方式。
```
 

65. DataReader与Dataset有什么区别？

```
答：一个是只能向前的只读游标，一个是内存中的表。
```
 

66. 软件开发过程一般有几个阶段？每个阶段的作用？

```
答：可行性分析(风险控制)，需求分析，架构设计，代码编写，测试，部署，维护
```
 

67. 在c#中using和new这两个关键字有什么意义，请写出你所知道的意义？using 指令和语句 new 创建实例 new 隐藏基类中方法。

```
答：using 引入名称空间或者使用非托管资源，使用完对象后自动执行实现了IDisposable接口的类的Dispose方法

    new 新建实例或者隐藏父类方法
```
 

68. 需要实现对一个字符串的处理,首先将该字符串首尾的空格去掉,如果字符串中间还有连续空格的话,仅保留一个空格,即允许字符串中间有多个空格,但连续的空格数不可超过一个.

```
答：string inputStr=" xx   xx  ";

inputStr=Regex.Replace(inputStr.Trim()," *"," ");
```
 

69. 下面这段代码输出什么？为什么？

```
int i=5;

int j=5;

if (Object.ReferenceEquals(i,j))

Console.WriteLine("Equal");

else

Console.WriteLine("Not Equal");
```

```
答：不相等，因为比较的是对象
```
 

70. 什么叫做SQL注入，如何防止？请举例说明。

```
答：利用sql关键字对网站进行攻击。过滤关键字'等
```
 

71. 什么是反射？

```
答：动态获取程序集信息
```
 

72. 用Singleton如何写设计模式

```
答：static属性里面new ,构造函数private
```
 

73. 什么是Application Pool？

```
答：Web应用，类似Thread Pool，提高并发性能。
```
 

74. 什么是虚函数？什么是抽象函数？

```
答：虚函数：没有实现的，可由子类继承并重写的函数。抽象函数：规定其非虚子类必须实现的函数，必须被重写。
```
 

75. 什么是XML？

```
答：XML即可扩展标记语言。eXtensible Markup Language.标记是指计算机所能理解的信息符号，通过此种标记，计算机之间可以处理包含各种信息的文章等。如何定义这些标记，即可以选择国际通用的标记语言，比如HTML，也可以使用象XML这样由相关人士自由决定的标记语言，这就是语言的可扩展性。XML是从SGML中简化修改出来的。它主要用到的有XML、XSL和XPath等。
```
 

76. 什么是Web Service？UDDI？

```
答：Web Service便是基于网络的、分布式的模块化组件，它执行特定的任务，遵守具体的技术规范，这些规范使得Web Service能与其他兼容的组件进行互操作。

　  UDDI 的目的是为电子商务建立标准；UDDI是一套基于Web的、分布式的、为Web Service提供的、信息注册中心的实现标准规范，同时也包含一组使企业能将自身提供的Web Service注册，以使别的企业能够发现的访问协议的实现标准。
```
 

77. 什么是ASP.net中的用户控件？

```
答：用户控件一般用在内容多为静态,或者少许会改变的情况下..用的比较大..类似ASP中的include..但是功能要强大的多。
```
 

78. 列举一下你所了解的XML技术及其应用

```
答：xml用于配置,用于保存静态数据类型.接触XML最多的是web Services..和config，存储经常使用但是不经常修改的数据
```
 

79. ADO.net中常用的对象有哪些？分别描述一下。

```
答：Connection 数据库连接对象

    Command 数据库命令

    DataReader 数据读取器

DataSet 数据集

DataAdaprer数据适配器
```
 

80. 什么是code-Behind技术。

```
答：ASPX,RESX和CS三个后缀的文件，这个就是代码分离.实现了HTML代码和服务器代码分离.方便代码编写和整理.
```
 

81. 什么是SOAP,有哪些应用。

```
答：simple object access protocal,简单对象接受协议.以xml为基本编码结构,建立在已有通信协议上(如http,不过据说ms在搞最底层的架构在tcp/ip上的soap)的一种规范Web Service使用的协议..
```
 

82. C#中 property 与 attribute的区别，他们各有什么用处，这种机制的好处在哪里？

```
答：一个是属性，用于存取类的字段，一个是特性，用来标识类，方法等的附加性质
```
 

83. XML 与 HTML 的主要区别

```
答：1. XML是区分大小写字母的，HTML不区分。

    2. 在HTML中，如果上下文清楚地显示出段落或者列表键在何处结尾，那么你可以省略</p>或者</li>之类的结束       标记。在XML中，绝对不能省略掉结束标记。

    3. 在XML中，拥有单个标记而没有匹配的结束标记的元素必须用一个 / 字符作为结尾。这样分析器就知道不用       查找结束标记了。

    4. 在XML中，属性值必须分装在引号中。在HTML中，引号是可用可不用的。

    5. 在HTML中，可以拥有不带值的属性名。在XML中，所有的属性都必须带有相应的值。
```
 

84. c#中的三元运算符是？

```
答：？：。
```
 

85. 当整数a赋值给一个object对象时，整数a将会被？

```
答：装箱。
```
 

86. 类成员有_____种可访问形式？

```
答：this.;new Class().Method;
```
 

87. public static const int A=1;这段代码有错误么？是什么？

```
答：const不能用static修饰。
```
 

88. float f=-123.567F; int i=(int)f;i的值现在是_____?

```
答：-123。
``` 

89. 委托声明的关键字是______?

```
答：delegate.
```
 

90. 用sealed修饰的类有什么特点？

```
答：密封，不能继承。
```
 

91. 在Asp.net中所有的自定义用户控件都必须继承自________?

```
答：Control。
```
 

92. 在.Net中所有可序列化的类都被标记为_____?

```
答：[serializable]
```
 

93. 在.Net托管代码中我们不用担心内存漏洞，这是因为有了______?

```
答：GC。
```
 

94. 下面的代码中有什么错误吗？_______

```
     using System;

     class A

     {

          public virtual void F(){

              Console.WriteLine("A.F");

           }

      }

      abstract class B:A

       {

           public abstract override void F();  / public abstract void F();
        // 答：abstract override 不可以一起修饰,应为 new public abstract void F();
       }                                           
```
 

95. 当类T只声明了私有实例构造函数时，则在T的程序文本外部，___可以___（可以 or 不可以）从T派生出新的类，不可以____（可以 or 不可以）直接创建T的任何实例。

```
答：不可以，不可以。
```
 

96. 下面这段代码有错误么？

    switch (i){

    case():        答：//case()条件不能为空

        CaseZero();

        break;

    case 1:

        CaseOne();

        break;

    case 2:

        dufault;   答：//wrong，格式不正确

        CaseTwo();

        break;

   }

 

97. 在.Net中，类System.Web.UI.Page 可以被继承么？

```
答：可以。
```
 

98. .net的错误处理机制是什么？

```
答：.net错误处理机制采用try->catch->finally结构，发生错误时，层层上抛，直到找到匹配的Catch为止。
```
 

99. 利用operator声明且仅声明了==，有什么错误么?

```
答：要同时修改Equale和GetHash() ? 重载了"==" 就必须重载 "!="
```
 

100. 在.net（C# or vb.net）中如何用户自定义消息，并在窗体中处理这些消息。

```
答：在form中重载DefWndProc函数来处理消息:

protected override void DefWndProc ( ref System.WinForms.Message m )

{

  switch(m.msg)

  {

    case WM_Lbutton :

　  ///string与MFC中的CString的Format函数的使用方法有所不同

　  string message = string.Format("收到消息!参数为:{0},{1}",m.wParam,m.lParam);

　  MessageBox.Show(message);///显示一个消息框

　  break;

    case USER:

    处理的代码

    default:

　  base.DefWndProc(ref m);///调用基类函数处理非自定义消息。

　  break;

  }

}
```
 

101. 在.net（C# or vb.net）中如何取消一个窗体的关闭。

```
答：private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)

   {

      e.Cancel=true;

   }
```
 

102. 在.net（C# or vb.net）中，Appplication.Exit 还是 Form.Close有什么不同？（winform题目非asp.net题目）

```
答：一个是退出整个应用程序，一个是关闭其中一个form。
```
 

103. 在C#中有一个double型的变量，比如10321.5，比如122235401.21644，作为货币的值如何按各个不同国家的习惯来输出。比如美国用$10,321.50和$122，235，401.22而在英国则为￡10  321.50和￡122  235  401.22

```
答：System.Globalization.CultureInfo MyCulture = new System.Globalization.CultureInfo("en-US");

    //System.Globalization.CultureInfo MyCulture = new System.Globalization.CultureInfo("en-GB");为英    国  货币类型

    decimal y = 9999999999999999999999999999m;

    string str = String.Format(MyCulture,"My amount = {0:c}",y);
```
 

104. 某一密码仅使用K、L、M、N、O共5个字母，密码中的单词从左向右排列，密码单词必须遵循如下规则：

（1）       密码单词的最小长度是两个字母，可以相同，也可以不同

（2）       K不可能是单词的第一个字母

（3）       如果L出现，则出现次数不止一次

（4）       M不能使最后一个也不能是倒数第二个字母

（5）       K出现，则N就一定出现

（6）       O如果是最后一个字母，则L一定出现

问题一：下列哪一个字母可以放在LO中的O后面，形成一个3个字母的密码单词？

  A) K   B)L   C) M    D) N

```
答案:B
```
 

问题二：如果能得到的字母是K、L、M，那么能够形成的两个字母长的密码单词的总数是多少？

  A)1个  B)3个  C)6个  D）9个

```
答案:A
```
 

问题三：下列哪一个是单词密码？

  A) KLLN   B) LOML   C) MLLO   D)NMKO

```
答案:C
```
 
 

105. 对于这样的一个枚举类型：

```
enum Color:byte

{

  Red,

  Green,

  Blue,

  Orange

}
```
```
答：string[] ss=Enum.GetNames(typeof(Color));

    byte[] bb=Enum.GetValues(typeof(Color));
```
 

106. C#中 property 与 attribute的区别，他们各有什么用处，这种机制的好处在哪里？

```
答：attribute:自定义属性的基类;property :类中的属性
```
 

107. C#可否对内存进行直接的操作？

```
答：在.net下，.net引用了垃圾回收（GC）功能，它替代了程序员  不过在C#中，不能直接实现Finalize方法，而是在析构函数中调用基类的Finalize()方法
```
 

108. ADO。NET相对于ADO等主要有什么改进？

```
答：1:ado.net不依赖于ole db提供程序,而是使用.net托管提供的程序,2:不使用com3:不在支持动态游标和服务器端游 4:,可以断开connection而保留当前数据集可用 5:强类型转换 6:xml支持
```
 

109. 写一个HTML页面，实现以下功能，左键点击页面时显示“您好”，右键点击时显示“禁止右键”。并在2分钟后自动关闭页面。

```
答：<script language=javascript>

 setTimeout('window.close();',3000);

 function show()

 {

  if (window.event.button == 1)

  {

   alert("左");

  }

  else if (window.event.button == 2)

  {

   alert("右");

  }

 }

</script>
```
 

110. 大概描述一下ASP。NET服务器控件的生命周期

```
答：初始化  加载视图状态  处理回发数据  加载  发送回发更改通知  处理回发事件  预呈现  保存状态  呈现  处置  卸载
```
 

111. Anonymous Inner Class (匿名内部类) 是否可以extends(继承)其它类，是否可以implements(实现)interface(接口)?

```
答：不能，可以实现接口
```
 

112. Static Nested Class 和 Inner Class的不同，说得越多越好

```
答：Static Nested Class是被声明为静态（static）的内部类，它可以不依赖于外部类实例被实例化。而通常的内部类需要在外部类实例化后才能实例化。
```
 

113. &和&&的区别。

```
&是位运算符，表示按位与运算，&&是逻辑运算符，表示逻辑与（and）.
```
 

114. HashMap和Hashtable的区别。

```
答：HashMap是Hashtable的轻量级实现（非线程安全的实现），他们都完成了Map接口，主要区别在于HashMap允许空（null）键值（key）,由于非线程安全，效率上可能高于Hashtable.
```
 

115. short s1 = 1; s1 = s1 + 1;有什么错? short s1 = 1; s1 += 1;有什么错?

```
答：short s1 = 1; s1 = s1 + 1; （s1+1运算结果是int型，需要强制转换类型）

short s1 = 1; s1 += 1;（可以正确编译）
```
 

116. Overloaded的方法是否可以改变返回值的类型?

```
答：Overloaded的方法是可以改变返回值的类型。
```
 

117. error和exception有什么区别?

```
答：error 表示恢复不是不可能但很困难的情况下的一种严重问题。比如说内存溢出。不可能指望程序能处理这样的情况。

exception 表示一种设计或实现问题。也就是说，它表示如果程序运行正常，从不会发生的情况。
```
 

118. <%# %> 和 <%  %> 有什么区别？

```
答：<%# %>表示绑定的数据源

    <% %>是服务器端代码块
```
 

119. 你觉得ASP.NET 2.0（VS2005）和你以前使用的开发工具（.Net 1.0或其他）有什么最大的区别？你在以前的平台上使用的哪些开发思想（pattern / architecture）可以移植到ASP.NET 2.0上 (或者已经内嵌在ASP.NET 2.0中)

```
答：1  ASP.NET 2.0 把一些代码进行了封装打包,所以相比1.0相同功能减少了很多代码.

    2  同时支持代码分离和页面嵌入服务器端代码两种模式,以前1.0版本,.NET提示帮助只有在分离的代码文件,无       法在页面嵌入服务器端代码获得帮助提示,

    3 代码和设计界面切换的时候,2.0支持光标定位.这个我比较喜欢

    4 在绑定数据,做表的分页.UPDATE,DELETE,等操作都可以可视化操作,方便了初学者

    5 在ASP.NET中增加了40多个新的控件,减少了工作量
```
 

120.重载与重写的区别？

```
答：1、方法的重写是子类和父类之间的关系，是垂直关系；方法的重载是同一个类中方法之间的关系，是水平关系

    2、重写只能由一个方法，或只能由一对方法产生关系；方法的重载是多个方法之间的关系。

    3、重写要求参数列表相同；重载要求参数列表不同。

    4、重写关系中，调用那个方法体，是根据对象的类型（对象对应存储空间类型）来决定；重载关系，是根据调       用时的实参表与形参表来选择方法体的。
```
 

121. 描述一下C#中索引器的实现过程，是否只能根据数字进行索引？

```
答：不是。可以用任意类型。
``` 

122. 在C＃中，string str = null 与 string str = " " 请尽量使用文字或图象说明其中的区别。

```
答：null是没有空间引用的；

    " " 是空间为0的字符串；
```
 

123. 分析以下代码，完成填空

```
string strTmp = "abcdefg某某某";

int i= System.Text.Encoding.Default.GetBytes(strTmp).Length;

int j= strTmp.Length;
```
以上代码执行完后，i= j=

```
答：i=13.j=10
``` 

124. SQLSERVER服务器中，给定表 table1 中有两个字段 ID、LastUpdateDate，ID表示更新的事务号， LastUpdateDate表示更新时的服务器时间，请使用一句SQL语句获得最后更新的事务号

```
答：Select ID FROM table1 Where LastUpdateDate = (Select MAX(LastUpdateDate) FROM table1)
```

125. 分析以下代码。

```
public static void test(string ConnectString)

 

{

 

System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();

conn.ConnectionString = ConnectString;

try

 

{

conn.Open();

…….

}

catch(Exception Ex)

{

MessageBox.Show(Ex.ToString());

}

finally

{

 

if (!conn.State.Equals(ConnectionState.Closed))

conn.Close();

}

}
```
请问

 

1)以上代码可以正确使用连接池吗？

 
```
答：回答：如果传入的connectionString是一模一样的话，可以正确使用连接池。不过一模一样的意思是，连字符的空格数，顺序完全一致。
```
 

2)以上代码所使用的异常处理方法，是否所有在test方法内的异常都可以被捕捉并显示出来？

 
```
答：只可以捕捉数据库连接中的异常吧. （finally中，catch中，如果有别的可能引发异常的操作，也应该用try,catch。所以理论上并非所有异常都会被捕捉。）
```
 

126. 公司要求开发一个继承System.Windows.Forms.ListView类的组件，要求达到以下的特殊功能：点击ListView各列列头时，能按照点击列的每行值进行重排视图中的所有行 (排序的方式如DataGrid相似)。根据您的知识，请简要谈一下您的思路：

```
答：根据点击的列头,包该列的ID取出,按照该ID排序后,在给绑定到ListView中
```
 

127. 什么是WSE？目前最新的版本是多少？

```
答：WSE (Web Service Extension) 包来提供最新的WEB服务安全保证，目前最新版本2.0。
```
 

128.在下面的例子里

```
     using System;

     class A

     {

          public A(){

                PrintFields();

           }

          public virtual void PrintFields(){}

      }

      class B:A

      {

           int x=1;

           int y;

           public B(){

               y=-1;

           }

           public override void PrintFields(){

               Console.WriteLine("x={0},y={1}",x,y);

           }
```

当使用new B()创建B的实例时，产生什么输出？

```
答：X=1,Y=0
```
 

129. 下面的例子中

 ```

     using System;

     class A

     {

          public static int X;

          static A(){

               X=B.Y+1;

          }

      }

      class B

      {

           public static int Y=A.X+1;

           static B(){}

           static void Main(){

                Console.WriteLine("X={0},Y={1}",A.X,B.Y);

           }

       }
```
产生的输出结果是什么？

```
答：x=1,y=2
```
 

130. 抽象类`abstract class`和接口`interface`有什么区别?

```
答：声明方法的存在而不去实现它的类被叫做抽象类（abstract class），它用于要创建一个体现某些基本行为的类，并为该类声明方法，但不能在该类中实现该类的情况。不能创建abstract 类的实例。然而可以创建一个变量，其类型是一个抽象类，并让它指向具体子类的一个实例。不能有抽象构造函数或抽象静态方法。Abstract 类的子类为它们父类中的所有抽象方法提供实现，否则它们也是抽象类为。取而代之，在子类中实现该方法。知道其行为的其它类可以在类中实现这些方法。

接口（interface）是抽象类的变体。在接口中，所有方法都是抽象的。多继承性可通过实现这样的接口而获得。接口中的所有方法都是抽象的，没有一个有程序体。接口只可以定义static final成员变量。接口的实现与子类相似，除了该实现类不能从接口定义中继承行为。当类实现特殊接口时，它定义（即将程序体给予）所有这种接口的方法。然后，它可以在实现了该接口的类的任何对象上调用接口的方法。由于有抽象类，它允许使用接口名作为引用变量的类型。通常的动态联编将生效。引用可以转换到接口类型或从接口类型转换，instanceof 运算符可以用来决定某对象的类是否实现了接口。

 



``` 

 

 
131. asp.net中web应用程序获取数据的流程：

```
A.Web Page　B.Fill  C.Sql05  D.Data Sourse  E.DataGrid  F.DataSet  G.Select and Connect

 

Commands  H.Sql Data Adapter
```
 
```
答案：a,e,d,f,h,g,b,c
```
 

132. Asp.net执行模式中各组件填入到对应位置：

```
A.Output Cache  B.Parser  C.Assembly Cache  D.IE  E.Memory  F.Compiler

WebPage被第一次请求时：

D->__->__->__->__->D

WebPage被第二次请求时：

D->__->__->D

WebPage的页面输出缓存被打开时：

D->__->D
```
 
```
答案：

WebPage被第一次请求时：

D->_b_->_f_->_a_->_e_->D

WebPage被第二次请求时：

D->_b_->_e_->D

WebPage的页面输出缓存被打开时：

D->_a_->D
```
 

133. 两个数组  [n] [m]  n>m  第一个数组的数字无序排列第二个数组为空取出第一个数组的最小值放到第二个数组中第一个位置, 依次类推. 不能改变A数组，不能对之进行排序，也不可以倒到别的数组中。

```
答：
int[] a = { -20, 9, 7, 37, 38, 69, 89, -1, 59, 29, 0, -25, 39, 900, 22, 13, 55 };

int[] b = new int[10];

int intTmp = a[0], intMaxNum;

for (int i = 0; i < a.Length; i++)

{

    intTmp = a[i] > intTmp ? a[i] : intTmp;

}

intMaxNum = intTmp;

for (int j = 0; j < b.Length; j++)

{



    for (int i = 0; i < a.Length; i++)

    {

        if (j == 0)

            intTmp = a[i] < intTmp ? a[i] : intTmp;

        else

        {

            if (a[i] > b[j - 1])

                intTmp = a[i] < intTmp ? a[i] : intTmp;

        }

    }

    b[j] = intTmp;

    intTmp = intMaxNum;

}

foreach (int bb in b)

{

    Console.WriteLine(bb);



}

Console.ReadLine();

```
 

134. 请将字符串`"I am a student"`按单词逆序输出如`"student a am I"`

```
string S = "I am a student";

char[] C = new char[] { ' '};

string[] n =S.Split(C);

int length = S.Length;

for (int i =length-1 ; i >=0; i--)

{

    Console.Write(n[i]);

    if (i != 0)

    {

        Console.Write(" ");

    }

}
```

135. 类和结构的区别？

```
1)	类是引用类型，结构是值类型。
2)	类可以继承，也可以实现接口；结构不能继承，但可以实现接口。
3)	类可以有显式声明的无参数构造函数；结构不能包含显式的无参数构造函数。
4)	结构中不能有实例字段初始值设定，但类没有任何限制。
5)	类的构造函数里面可以不用对所有字段进行初始化；结构的构造函数里面必须对所有字段进行初始化。
6)	类可以有析构函数，但结构不行。

```

136. 接口可以包含哪些成员？

```
方法、属性、事件、索引器
```

137. C#中，哪些类型是值类型？哪些类型是引用类型？

```
值类型：结构、枚举。
引用类型：类、接口、委托、数组、字符串。

```

138. C#中的基本数据类型有哪些？

```
值类型：
    有符号整数（由小到大）：sbyte、short、int、long
    无符号整数（由小到大）：byte、ushort、uint、ulong
    浮点型：float、double、decimal
    布尔型：bool
    字符型：char
引用类型：
    string、object

```
139. 构造函数是否可以被继承？是否可以被重写？

```
不能被继承，也不能被重写，但是在创建类的实例时会首先调用其基类的无参数构造函数。
```
140. C#中，运算符“?”和“??”的区别是什么？

```
?是和:结合在一起使用的，?:称为条件运算符（或三目运算符），该运算符根据布尔型表达式的值返回两个值之一。条件运算符的格式如下：
condition ? first_expression : second_expression;
如果条件condition为 true，则计算第一表达式first_expression并以它的计算结果为准；如果为 false，则计算第二表达式second_expression并以它的计算结果为准。只会计算两个表达式其中的一个。
?? 运算符称为 null 合并运算符，用于定义可以为 null 值的类型和引用类型的默认值。如果此运算符的左边不为 null，则此运算符将返回左边的值；如果此运算符的左边为null，则返回右边的值。

```
141. C#中，用const和readonly修饰的字段有什么区别？

```
const：表示常量，其修饰的值是在编译期间确定的，因此在声明时只能通过常量表达式指定其值。
readonly：表示只读字段，其修饰的字段的值是在运行时计算的，在对象的实例构造函数或类的静态构造函数首次被调用时计算，之后字段的值便不能被更改。

```
142. string s = ”” 和string s = null的区别？string  s,string s=””,string s=null,string s=string.Empty.

```
string s = “” ：字符串变量s指向了空字符串，表示给变量s分配了内存空间，只不过s指向了一个长度为零的空字符串对象。
string s = null ：字符串变量s指向了空引用null，表示s没有引用任何对象，即没有给变量s分配内存空间。

```
143. C#中，new有几种用法？

```
1)	用于创建对象和调用构造函数。
2)	用于创建匿名类型的实例。
3)	在用作修饰符时，new 关键字可以显式隐藏从基类继承的成员。

```
144. C#中，ref和out在修饰方法参数时有什么区别？ 

```
ref和out都可以实现将一个指定的参数按照引用进行传递：
    ref参数在传入方法之前必须进行初始化；而out参数在传入方法之前可以不用初始化。
    ref参数在方法内部可以直接使用；而out参数在方法内部不能直接使用。
    ref参数在方法内部可以使用也可以不使用；而out参数在方法返回之前必须对其赋值。

```
145. C#中，using有几种用法？ 

```
1)	导入和引用命名空间。
2)	给类型起别名。
3)	使用using声明的对象，可以确保在using代码块结束时，该对象所使用的资源被自动释放。

```
146. C#中，&和&&，|和||有什么区别？

```
&：按位与，表示对两个整数进行按位与运算。
&&：逻辑与，表示对两个布尔型进行逻辑与操作，即当且仅当两边的条件都为true时，表达式才返回true。
|：按位或，表示对两个整数进行按位或运算。
||：逻辑或，表示对两个布尔型进行逻辑或操作，即只要两边的条件中有一个为true，表达式就返回true。

```
147. C#中，is和as有什么区别？

```
is：用于检查对象是否与给定的类型兼容。
as：用于对引用类型的变量进行类型转换。

```
148. ADO.NET中访问数据库的基本步骤是什么？ 

```
创建数据库连接对象。
打开连接。
创建命令对象，执行SQL语句。
关闭连接。

```
149. ADO.NET中，DataSet和DataReader的区别是什么？ 

```
DataSet(断开式的)：数据集，表示内存中的“数据库”。DataSet在从数据库中读取数据时是“面向非连接的”，即DataSet在读取数据的那一刻会与数据库进行连接，然后会一次性把数据库中所有的表以及数据读取到内存中，然后便断开数据库连接。
DataReader(连接式的)：数据读取器。DataReader在从数据库中读取数据时是“面向连接的”，即DataReader在读取数据时是从数据库中一行一行读取，每次只会从数据库中读取一行数据（类似于数据库游标的行为），直到读完最后一行之后，才断开数据库连接。在整个读取的过程中，必须保持与数据库的连接处于打开状态。

```
150. ADO.NET中常用对象有哪些？

```
SqlConnection：连接对象，用于执行与数据库的连接。
SqlCommand：命令对象，用于对数据库执行SQL语句。
SqlDataAdapter：适配器对象，用于填充数据集和更新数据库。
SqlParameter：参数对象，用于执行参数化SQL语句。
SqlDataReader：读取器对象，用于从数据库中快速逐行读取数据。
SqlTransaction：事务对象，用于执行数据库事务。


```
151. 进程和线程的区别？

```
进程是系统进行资源分配和调度的单位；线程是 CPU 调度和分派的单位，一个进程可以有多个线程，这些线程共享这个进程的资源。

```
152. 什么是反射？

```
动态获取程序集信息

```
153. HashMap 和 Hashtable 的区别。 

```
HashMap 是 Hashtable 的轻量级实现（非线程安全的实现），他们都完成了 Map 接口，主要区别在于 HashMap 允许空（ null ）键值（ key ） , 由于非线程安全，效率上可能高于 Hashtable.

```
154. 概述反射和序列化?

```
反射：要给发射下一个定义还是比较难的，这里先说说我的理解。反射提供了封装程序集，模块和类型对象，可以用反射动态地创建类型的实例，将类型绑定到现有对象，或者从现有对象类型里获取类型，然后调用类型的方法或访问字段和属性。
序列化：将对象转换为另一种媒介传输的格式过程。如，序列化一个对象，用Http通过internet在客户端和服务器之间传递该对象，在另一端用反序列化从该流中重新得到对象。目前主流的序列化是把一个对象序列化为一个json字符串。

```
## 二、Asp.Net

1. ASP.NET中的身份验证有那些？

```
1、Windows验证：ASP.NET会结合Internet信息服务（IIS），为每个用户开启Windows账户，通过Windows账户验证用户身份。
2、Forms验证：ASP.NET通过Web表单，为每个用户创建一个用cookie保存的身份验证票据，通过该票据验证用户身份。
3、Passport验证：由 Microsoft 提供的集中身份验证服务，通过将每个用户的信息提交给Passport 登录服务网站进行用户身份的验证。
4、None验证：自定义身份验证方式。

```
2. Server.Transfer与Response.Redirect有什么区别？

```
Server.Transfer：服务器端重定向，在服务器内部将请求从A页面重定向到B页面。由于是在服务器内部进行重定向，浏览器端并不知道服务器内部发生了重定向，因此浏览器的地址栏显示的URL不变，仍是最初请求的A页面。服务器端重定向不能跨站点。
Response.Redirect：客户端重定向，服务器向客户端浏览器发送一个重定向信号和重定向的URL地址，浏览器收到该信号后，重新去请求要重定向的URL地址。由于重定向是发生在浏览器端，因此浏览器的地址栏会变成重定向后的URL。浏览器端重定向可以跨站点。

```
3. ASP.NET 中页面之间传递值的几种方式？

```
QueryString（查询字符串）：通过URL中的查询字符串（Default.aspx?id=5&name=abc）传值。
优点：简单便捷。
缺点：安全性差，且长度大小有限制。

Session：通过会话状态传值。
优点：当前会话中的所有页面均可使用，且能够传递任何对象。
缺点：可能会丢失（Session过期或失效等情况）。

Cookie：通过客户端Cookie传值。
优点：读取该Cookie的所有页面都可以使用。
缺点：只能够存储文本信息（字符串），且大小不能超过4KB。

Application：通过全局应用程序对象传值。
优点：整个应用程序都可以使用，且能够传递任何对象。
缺点：可能会产生并发问题。

Server.Transfer：通过服务器端重定向传值。
优点：可以将与最初请求相关的所有数据传递给重定向的页面。
缺点：资源消耗比较大。

```
4. 什么是“Code-Behind”技术？

```
Code-Behind：代码隐藏技术（代码后置），在ASP.NET中aspx页面通过其页面指令@Page，将页面的处理逻辑代码分离到另一个cs文件中，从而将页面的显示逻辑和处理逻辑分离，提高了页面的可维护性，并且不同职责的开发人员（程序员、美工）可以并行开发，提高了开发效率。
```
5. ASP.NET中调用Web Service的方法有哪些？

```
通过HTTP-GET请求调用。
通过HTTP-POST请求调用。
通过SOAP请求调用。

```
6. 怎样理解web应用程序的“无状态编程”？

```
Web应用程序是基于HTTP协议的，而HTTP协议具有无状态性。即客户端向Web服务器发送HTTP请求，服务器接收到请求，将相应的内容发送给客户端。在这一过程中，服务器不会主动记录任何有关客户端请求的信息，在本次请求结束后，服务器也不会保存有关本次请求的任何信息——所有的请求、响应都是即时的，当请求结束后，与本次请求相关的所有资源都将被释放。所以当客户端下一次向Web服务器发送请求时，服务器并不知道该客户端之前有没有发送过请求，也不知道本次请求是第几次请求。这便是Web应用程序的无状态性。
```
7. Session有什么缺点？可以使用什么方法加以解决？

```
Session缺点：默认情况下Session的存储采用的是“InProc”配置，即存储在IIS进程内存中，当IIS重启或发生进程回收时，Session则会丢失。
解决方法：可以将Session配置为“StateServer”或“SQLServer”——存储在状态服务进程或数据库中，可以避免丢失，但此时无法捕获Session_End事件。

```
8. ASP.NET中所有自定义用户控件都必须继承自哪一个类？页面必须继承哪一个类？

```
用户控件基类：System.Web.UI.UserControl。
页面基类：System.Web.UI.Page。

```
9. 向服务器发送请求有几种方式？

```
GET：向服务器请求数据。
POST：向服务器提交数据。
PUT:向指定资源位置上传其最新内容
DELETE:请求服务器删除Request-URL所标识的资源
OPTIONS:返回服务器针对特定资源所支持的HTTP请求方法，也可以利用向web服务器发送‘*’的请求来测试服务器的功能性
HEAD:向服务器索与GET请求相一致的响应，只不过响应体将不会被返回。这一方法可以再不必传输整个响应内容的情况下，就可以获取包含在响应小消息头中的元信息。
CONNECT:HTTP/1.1协议中预留给能够将连接改为管道方式的代理服务器。
TRACE:回显服务器收到的请求，主要用于测试或诊断

```
10. Web Service有哪些优点？

```
Web Service是基于XML的，与具体的语言和平台无关，故可以实现跨平台、跨语言通信。
Web Service是采用SOAP协议（简单对象访问协议）进行通信的，该协议基于HTTP协议，故可以实现跨防火墙通信。
Web Service可以很容易的实现分布式的应用程序。

```
11. ASP.NET缓存有几种实现方式？

```
页面输出缓存：将页面全部进行缓存。
页面局部缓存：将页面中的一部分放在用户控件中，对该部分进行缓存。
数据缓存：使用Cache类进行数据缓存。
客户端缓存：使用HttpCachePolicy类进行客户端（浏览器）缓存。

```
12. 什么是ViewState？有什么作用？

```
ViewState：视图状态。
Web 应用程序是无状态的。每次从服务器请求网页时，都会创建网页类的一个新实例。这通常意味着在每次往返过程中将会丢失所有与该页面及其控件关联的信息。为了克服 Web 编程的这一固有局限性，ASP.NET 页框架包含了状态管理功能，可以在往返过程之间保存页和控件的相关联的信息及其值。这便是视图状态。


```
13. ASP.NET中常见文件的后缀名有哪些？

```
aspx：页面
ascx：用户控件
asmx：Web Service
ashx：一般处理程序
asax：Global.asax，全局应用程序配置

```
14. ASP.NET中的六大对象有哪些？

```
Request、(请求对象)
Response、(响应对象)
Server、(服务器对象)
Session、(会话对象)
Cookie、
Application(应用程序对象)
```
15. jquery中实现Ajax请求的常用方法有哪些？有什么区别？

```
$.get方法：通过get方式发送请求。
$.post：通过post方式发送请求。
$.ajax：可以使用指定的方式来发送请求。$.ajax方法是jquery中发送Ajax请求的最底层方法，可以完全自定义发送请求时的相关参数。
$.getJSON()
```
16. 手动实现Ajax请求的步骤是什么？

```
创建XMLHttpRequest对象
调用open方法初始化请求
设置回调函数
发送请求

```
17. MVC中的各个字母分别代表什么含义？在MVC框架中起什么作用？

```
M：Model，模型，是应用程序的数据处理逻辑部分，用于数据的访问。
V：View，视图，是应用程序的数据显示部分，用于呈现页面。
C：Controller，控制器，是应用程序流程控制部分，用于处理请求逻辑，访问模型数据。

```
18. MVC请求的处理过程？

```
请求到达服务器后，首先进入路由系统进行验证，如果请求的Url地址格式符合路由的定义，则将请求交给控制器中的方法进行处理。在方法中，可能会访问模型中的数据、处理相应的业务逻辑，然后方法将根据请求的内容返回一个适当的视图呈现给客户端。
```
19. 请使用jquery实现一个复选框全选、反选的功能。

```

```
20. 请使用jquery实现表格中隔行变色的效果。

```
关键代码：$('#table tr:even').css('background-color','Orange')
         $('#table tr:odd').css('background-color','Pink')
```
21. 请使用jquery实现表格中鼠标移动的光棒效果。

```

```
22. 您了解设计模式么?请列出您所知道的设计模式的名称。

```
设计模式是一个设计经验的总结，我所知道的设计模式有结构工厂模式  单例模式  建造者模式 原型模式
```


## 三、SQL Server

1. 写出一条Sql语句： 取出表A中第31条到第40条记录（SQL Server, 以自动增长的ID作为主键,  注意：ID可能不是连续的）。

```
方法一：
select top 10 * from A where id not in 
(select top 30 id from A order by id) order by id
方法二：
	select top 10 * from A 
where id>(select max(id) from (
	select top 30 id from A order by id ) t)
方法三：
	select * from (
	select *,row_number() over (order by id) Row_No from A
) t where Row_No between 31 and 40 

```
2. 请说出SQL Server中三种表连接的方式inner join、left join、right join的区别及对最终查询的结果的影响。

```
inner join：内联查询，查询出满足on条件的两个表的公共交集。
left join：左外联查询，left outer join的简写形式，以左边的表为基准与右边的表进行关联，连接查询出满足on条件的结果，但左表的数据会完全保留，其中右表中无法满足on条件的数据会在连接的结果中用null与左表数据补齐。
right join：右外联查询，right outer join的简写形式，以右边的表为基准与左边的表进行关联，连接查询出满足on条件的结果，但右表的数据会完全保留，其中左表中无法满足on条件的数据会在连接的结果中用null与右表数据补齐。

```
3. 存储过程和函数的区别？

```
函数是可以嵌入在sql语句中使用的,比如函数可以作为查询语句的一个部分来调用；存储过程大多是作为一个独立的部分来执行，存储过程需要通过exec调用。
函数限制比较多，比如不能用临时表，只能用表变量等；而存储过程的限制相对就比较少，几乎所有的Sql代码都可以使用。
一般来说，存储过程实现了业务逻辑的功能，比较复杂；而函数实现的功能针对性比较强，用于完成单一目的功能。
对于存储过程来说可以返回一个或多个输出参数，也可以返回多个结果集，而函数只能返回一个值或者表对象。

```
4. 数据库索引是什么？有什么作用？

```
数据库索引：是数据库表中一列或多列的值进行排序的一种结构。
作用：索引类似于一本书的目录，主要用于加快从表中查询数据的速度。

```
5. 数据库索引的分类及区别？

```
聚集索引：表示索引中的结构与表中相应行的数据在物理磁盘上存储的顺序相同的索引。
非聚集索引：表示索引中的结构与表中相应行的数据在物理磁盘上存储的顺序不相同的索引。

```

6. 什么是事务？它有哪些特性？

```
事务：执行单个逻辑功能的一组操作称为事务。事务是一个最小的工作单元，不论执行成功与失败都作为一个整体进行工作。
事务的ACID特性：
原子性（Atomicity）：事务的执行是作为原子的，即不可再分。整个语句作为一个不可再分的执行单元，要么都执行，要么都不执行。
一致性（Consistency）：在事务开始之前和事务结束之后，所有的数据都保持一致状态。
隔离性（Isolation）：事务的执行是互不干扰的，一个事务不可能看到其他事务运行时，中间某一时刻的数据。
持久性（Durability）：事务完成之后，该事务所对数据库所作的更改便持久的保存在数据库之中，并不会被回滚。

```
7. 使用事务最大的好处是什么？

```
事务最大的好处就是可以保证数据的完整性和一致性，因为事务要么全部执行成功，要么全部执行不成功。
```
8. SQL Server中，触发器分为哪几种？分别代表什么含义？

```
SQL Server中触发器主要分为两大类：
DML触发器：当数据库中发生数据操作语言 (DML) 事件时将调用DML触发器。DML事件包括在指定表或视图中修改数据的 INSERT 语句、UPDATE 语句或 DELETE 语句。
DML触发器又分为：
1)	after触发器（之后触发）：在执行了 INSERT、UPDATE 或 DELETE 语句操作之后被触发。可以定义三种类型：INSERT触发器、UPDATE触发器、DELETE触发器。after触发器只能定义在表上。
2)	instead of 触发器 （之前触发）：instead of触发器也叫替代触发器，表示并不执行其定义的操作（INSERT、UPDATE、DELETE），而是仅执行触发器本身。既可以在表上定义instead of触发器，也可以在视图上定义。
DDL触发器：当数据库中发生数据定义语言 (DDL) 事件时将调用DDL触发器。DDL事件主要与以关键字 CREATE、ALTER 和 DROP 开头的SQL语句对应。

```
9. delete和truncate有什么区别？

```
delete可以带where条件，用于删除指定条件的数据；truncate和table组合在一起使用“truncate  table 表名”，truncate不能带where条件。
不带where条件的delete和truncate都用于删除表中的所有数据，但如果表中有自增长列，delete在删除所有数据之后，下一次插入新的数据时自增长列依然紧接着最后一次的数值的下一个；而truncate删除所有数据后，下一次插入新数据的自增长列的数值将被重置，重新开始。

```
10. union和union  all的区别？

```
union 和 union  all都可以用作两张表数据的合并：
union：在合并时会去除掉重复的记录。
union all：直接合并，对重复的记录不作处理。

```
11. SQL Server中，有一张学生成绩表，有三个字段：学生ID，学生姓名，考试成绩。如何查询出成绩排在前三名的学生？（注意：可能会有并列名次）

```
select * from (
	select *,dense_rank() over (order by 学生成绩 desc) No 
from 学生成绩表
) t where No<=3

SQL Server排名函数：row_number()、rank()、dense_rank()

row_number():排名连序
rank():排名不连序，遇值相同排名相同，并且占用接下来的排名数字
dense_rank():排名不连序，遇值相同排名相同，并且不占用接下来的排名数字

```
12. SQL Server中，向一个表中插入了新数据，如何得到自增长字段的当前值？

```
select  @@identity
```
13. SQL Server中，游标有什么作用？如何知道游标已经到了最后？

```
作用：从包括多条数据记录的结果集中每次提取一条记录。游标类似于程序代码中对集合的遍历循环，能够遍历结果中的所有行，在遍历过程中，每次只读取一行的数据。
当全局变量@@fetch_status的值不等于0时，表示游标已经到了最后。

```
14. 描述一下 C# 中索引器的实现过程，是否只能根据数字进行索引？

```
不是。可以用任意类型。
```

15. 如何处理几十万条并发数据？

```
用存储过程或事务。取得最大标识的时候同时更新 .. 注意主键不是自增量方式这种方法并发的时候是不会有重复主键的 .. 取得最大标识要有一个存储过程来获取
```

16. 什么叫做SQL注入，如何防止?请举例说明。

```
所谓SQL注入，就是通过把SQL命令插入到Web表单提交或输入域名或页面请求的查询字符串，最终达到欺骗服务器执行恶意的SQL命令,可以用sqldataparmert对象来进行防注式攻击
```

## 四、算法

1. 请使用递归算法来实现计算1+2+3+4+…+100的结果。

```
class Program
{
    static void Main(string[] args)
    {
        int total = Sum(100);
        Console.WriteLine(total);
    }

    static int Sum(int x)
    {
        if (x <= 1)
            return x;
        else
            return x + Sum(x - 1);
    }
}

```
2. 请实现一个冒泡排序算法。

```
public static void Sort(int[] nums)
{
    int temp;
    for (int i = 0; i < nums.Length - 1; i++)
    {
        for (int j = i + 1; j < nums.Length; j++)
        {
            if (nums[j] < nums[i])
            {
                temp = nums[i];
                nums[i] = nums[j];
                nums[j] = temp;
            }
        }
    }
}

```
3. 请实现一个方法，对于输入的任意字符串，统计出其中每一种字符出现的次数。 

```
public static void CountChar(string s)
{
    Dictionary<char, int> dic = new Dictionary<char, int>();
    foreach (char c in s)
    {
        if (dic.ContainsKey(c))
        {
            dic[c]++;
        }
        else
        {
            dic.Add(c, 1);
        }
    }
    foreach (KeyValuePair<char,int> p in dic)
    {
        Console.WriteLine("字符:{0}，出现次数：{1}", p.Key.ToString(), p.Value.ToString());
    }
}

```
4. 请手动实现一个将字符串转换为整数的方法，不要使用int.Parse、int.TryParse、Convert.ToInt32等任何类库方法。

```
public static bool TryParse(string s, out int num)
{
    if (string.IsNullOrEmpty(s))
    {
        num = 0;
        return false;
    }
    int result = 0;
    bool minus = s[0] == '-' ? true : false;
    if (minus && s.Length == 1)
    {
        num = 0;
        return false;
    }
    for (int i = minus ? 1 : 0; i < s.Length; i++)
    {
        if (s[i] >= '0' && s[i] <= '9')
        {
            result = (s[i] - 48) + result * 10;
        }
        else
        {
            num = 0;
            return false;
        }
    }
    num = minus ? -result : result;
    return true;
}

```
5. 一列数的规则如下 : 1 、 1 、 2 、 3 、 5 、 8 、 13 、 21 、 34......  求第 30 位数是多少，   用递归算法实现。

```
public class MainClass 
{ 
   public static void Main() 
   { 
     Console.WriteLine(Foo(30)); 
   } 
   public static int Foo(int i) 
   { 
     if (i <= 0) 
        return 0; 
     else if(i > 0 && i <= 2) 
        return 1; 
      else
       return Foo(i -1) + Foo(i - 2); 
    } 
} 

```
6. 求以下表达式的值，写出您想到的一种或几种实现方法：  1-2+3-4+……+m

```
int Num = this.TextBox1.Text.ToString() ;
int Sum = 0 ;
for (int i = 0 ; i < Num + 1 ; i++)
{
   if((i%2) == 1)
    {
     Sum += i ;
    }
    else
    {
     Sum = Sum - I ;
    }
}
System.Console.WriteLine(Sum.ToString());
System.Console.ReadLine() ;

```
7. 写一程序，把字符串“ABCDE”转为“EDCBA"

```
string name = "ABCDE";
char[] arry1 = name.ToCharArray();
string x ="";
for (int k = arry1.Length - 1; k >= 0; k--)
{
  x += arry1[k];
}
Console.WriteLine(x);

```

## 五、Vue

1. vue优点？

```
答：

轻量级框架：只关注视图层，是一个构建数据的视图集合，大小只有几十 kb ；
简单易学：国人开发，中文文档，不存在语言障碍 ，易于理解和学习；
双向数据绑定：保留了 angular 的特点，在数据操作方面更为简单；
组件化：保留了 react 的优点，实现了 html 的封装和重用，在构建单页面应用方面有着独特的优势；
视图，数据，结构分离：使数据的更改更为简单，不需要进行逻辑代码的修改，只需要操作数据就能完成相关操作；
虚拟DOM：dom 操作是非常耗费性能的， 不再使用原生的 dom 操作节点，极大解放 dom 操作，但具体操作的还是 dom 不过是换了另一种方式；
运行速度更快：相比较于 react 而言，同样是操作虚拟 dom ，就性能而言， vue 存在很大的优势。
```

2. vue父组件向子组件传递数据？

```
答：通过 props
```
3. 子组件像父组件传递事件？

```
答：$emit 方法
```
4. v-show 和 v-if指令的共同点和不同点？

```
答:

共同点： 都能控制元素的显示和隐藏；

不同点： 实现本质方法不同，v-show 本质就是通过控制 css 中的 display 设置为 none，控制隐藏，只会编译一次；v-if 是动态的向 DOM 树内添加或者删除 DOM 元素，若初始值为 false ，就不会编译了。而且 v-if 不停的销毁和创建比较消耗性能。总结：如果要频繁切换某节点，使用 v-show (切换开销比较小，初始开销较大)。如果不需要频繁切换某节点使用 v-if（初始渲染开销较小，切换开销比较大）。
```
5. 如何让CSS只在当前组件中起作用?

```
答：在组件中的 style 前面加上 scoped
```
6. <keep-alive></keep-alive> 的作用是什么?

```
答: keep-alive 是 Vue 内置的一个组件，可以使被包含的组件保留状态，或避免重新渲染。
```
7. 如何获取dom?

```
答：ref="domName" 用法：this.$refs.domName
```
8. 说出几种vue当中的指令和它的用法？

```
答：
v-model: 双向数据绑定；

v-for: 循环；

v-if、v-show: 显示与隐藏；

v-on: 事件；

v-once: 只绑定一次。
```
9. vue-loader是什么？使用它的用途有哪些？

```
答：vue 文件的一个加载器，将 template/js/style 转换成 js 模块。

用途：js 可以写 es6 、 style样式可以 scss 或 less 、 template 可以加 jade等
```
10. 为什么使用key?

```
答：需要使用 key 来给每个节点做一个唯一标识， Diff 算法就可以正确的识别此节点。作用主要是为了高效的更新虚拟 DOM。
```
11. axios及安装?
```
答：请求后台资源的模块。npm install axios —save 装好， js中使用 import 进来，然后 .get 或 .post 。返回在 .then 函数中如果成功，失败则是在 .catch 函数中。
```
12. v-model的使用

```
答：v-model 用于表单数据的双向绑定，其实它就是一个语法糖，这个背后就做了两个操作：v-bind 绑定一个 value 属性；v-on 指令给当前元素绑定 input 事件。
```
13. 请说出vue.cli项目中src目录每个文件夹和文件的用法？

```
答：assets 文件夹是放静态资源；components 是放组件；router 是定义路由相关的配置;  app.vue 是一个应用主组件；main.js 是入口文件。
```
14. 分别简述computed和watch的使用场景

```
答：

computed : 当一个属性受多个属性影响的时候就需要用到 computed ，最典型的栗子：购物车商品结算的时候
watch : 当一条数据影响多条数据的时候就需要用 watch ，栗子：搜索数据
```
15. v-on可以监听多个方法吗？

```
答：可以，栗子：<input type="text" v-on="{ input:onInput,focus:onFocus,blur:onBlur, }">。
```
16. $nextTick的使用

```
答：当你修改了data 的值然后马上获取这个 dom 元素的值，是不能获取到更新后的值， 你需要使用 $nextTick 这个回调，让修改后的 data 值渲染更新到 dom 元素之后在获取，才能成功。
```
17. vue组件中data为什么必须是一个函数？

```
答：因为 JavaScript 的特性所导致，在 component 中，data 必须以函数的形式存在，不可以是对象。组建中的 data 写成一个函数，数据以函数返回值的形式定义，这样每次复用组件的时候，都会返回一份新的 data ，相当于每个组件实例都有自己私有的数据空间，它们只负责各自维护的数据，不会造成混乱。而单纯的写成对象形式，就是所有的组件实例共用了一个 data ，这样改一个全都改了。
```
18. 渐进式框架的理解

```
答：主张最少；可以根据不同的需求选择不同的层级；
```
19. Vue中双向数据绑定是如何实现的？

```
答：vue 双向数据绑定是通过 数据劫持 结合 发布订阅模式的方式来实现的， 也就是说数据和视图同步，数据发生变化，视图跟着变化，视图变化，数据也随之发生改变；核心：关于VUE双向数据绑定，其核心是  Object.defineProperty() 方法。
```
20. 单页面应用和多页面应用区别及优缺点

```
答：单页面应用（SPA），通俗一点说就是指只有一个主页面的应用，浏览器一开始要加载所有必须的 html, js, css。所有的页面内容都包含在这个所谓的主页面中。但在写的时候，还是会分开写（页面片段），然后在交互的时候由路由程序动态载入，单页面的页面跳转，仅刷新局部资源。多应用于pc端。

多页面（MPA），就是指一个应用中有多个页面，页面跳转时是整页刷新

单页面的优点：用户体验好，快，内容的改变不需要重新加载整个页面，基于这一点spa对服务器压力较小；前后端分离；页面效果会比较炫酷（比如切换页面内容时的专场动画）。

单页面缺点：不利于seo；导航不可用，如果一定要导航需要自行实现前进、后退。（由于是单页面不能用浏览器的前进后退功能，所以需要自己建立堆栈管理）；初次加载时耗时多；页面复杂度提高很多。
```
21. v-if和v-for的优先级

```
答：当 v-if 与 v-for 一起使用时，v-for 具有比 v-if 更高的优先级，这意味着 v-if 将分别重复运行于每个 v-for 循环中。所以，不推荐 v-if 和 v-for 同时使用。如果 v-if 和 v-for 一起用的话，vue中的的会自动提示 v-if 应该放到外层去。
```
22. assets和static的区别

```
答：相同点： assets 和 static 两个都是存放静态资源文件。项目中所需要的资源文件图片，字体图标，样式文件等都可以放在这两个文件下，这是相同点

不相同点：assets 中存放的静态资源文件在项目打包时，也就是运行 npm run build 时会将 assets 中放置的静态资源文件进行打包上传，所谓打包简单点可以理解为压缩体积，代码格式化。而压缩后的静态资源文件最终也都会放置在 static 文件中跟着 index.html 一同上传至服务器。static 中放置的静态资源文件就不会要走打包压缩格式化等流程，而是直接进入打包好的目录，直接上传至服务器。因为避免了压缩直接进行上传，在打包时会提高一定的效率，但是 static 中的资源文件由于没有进行压缩等操作，所以文件的体积也就相对于 assets 中打包后的文件提交较大点。在服务器中就会占据更大的空间。

建议： 将项目中 template需要的样式文件js文件等都可以放置在 assets 中，走打包这一流程。减少体积。而项目中引入的第三方的资源文件如iconfoont.css 等文件可以放置在 static 中，因为这些引入的第三方文件已经经过处理，我们不再需要处理，直接上传。
```
23. vue常用的修饰符

```
答：

.stop：等同于 JavaScript 中的 event.stopPropagation() ，防止事件冒泡；
.prevent ：等同于 JavaScript 中的 event.preventDefault() ，防止执行预设的行为（如果事件可取消，则取消该事件，而不停止事件的进一步传播）；
.capture ：与事件冒泡的方向相反，事件捕获由外到内；
.self ：只会触发自己范围内的事件，不包含子元素；
.once ：只会触发一次。
```
24. vue的两个核心点

```
答：数据驱动、组件系统

数据驱动： ViewModel，保证数据和视图的一致性。

组件系统： 应用类UI可以看作全部是由组件树构成的。
```
25. vue和jQuery的区别

```
答：jQuery是使用选择器（ $ ）选取DOM对象，对其进行赋值、取值、事件绑定等操作，其实和原生的HTML的区别只在于可以更方便的选取和操作DOM对象，而数据和界面是在一起的。比如需要获取label标签的内容：$("lable").val(); ,它还是依赖DOM元素的值。Vue则是通过Vue对象将数据和View完全分离开来了。对数据进行操作不再需要引用相应的DOM对象，可以说数据和View是分离的，他们通过Vue对象这个vm实现相互的绑定。这就是传说中的MVVM。
```
26. 引进组件的步骤

```
答: 在template中引入组件；在script的第一行用import引入路径；用component中写上组件名称。
```
27. delete和Vue.delete删除数组的区别

```
答：delete 只是被删除的元素变成了 empty/undefined 其他的元素的键值还是不变。Vue.delete 直接删除了数组 改变了数组的键值。
```
28. SPA首屏加载慢如何解决

```
答：安装动态懒加载所需插件；使用CDN资源。
```
29. Vue-router跳转和location.href有什么区别

```
答：使用 location.href= /url 来跳转，简单方便，但是刷新了页面；使用 history.pushState( /url ) ，无刷新页面，静态跳转；引进 router ，然后使用 router.push( /url ) 来跳转，使用了 diff 算法，实现了按需加载，减少了 dom 的消耗。其实使用 router 跳转和使用 history.pushState() 没什么差别的，因为vue-router就是用了 history.pushState() ，尤其是在history模式下。
```
30. 插槽(slot)

```
答：简单来说，假如父组件需要在子组件内放一些DOM，那么这些DOM是显示、不显示、在哪个地方显示、如何显示，就是slot分发负责的活。
```
31. 你们vue项目是打包了一个js文件，一个css文件，还是有多个文件？

```
答：根据vue-cli脚手架规范，一个js文件，一个CSS文件。
```
32. Vue里面router-link在电脑上有用，在安卓上没反应怎么解决？

```
答：Vue路由在Android机上有问题，babel问题，安装babel polypill插件解决。
```
33. Vue2中注册在router-link上事件无效解决方法
答：使用 @click.native 。原因：router-link会阻止click事件，.native指直接监听一个原生事件。

34. RouterLink在IE和Firefox中不起作用（路由不跳转）的问题

```
答: 方法一：只用a标签，不适用button标签；方法二：使用button标签和Router.navigate方法
```
35. axios的特点有哪些

```
答：

从浏览器中创建XMLHttpRequests；

node.js创建http请求；

支持Promise API；

拦截请求和响应；

转换请求数据和响应数据；

取消请求；

自动换成json。

axios中的发送字段的参数是data跟params两个，两者的区别在于params是跟请求地址一起发送的，data的作为一个请求体进行发送

params一般适用于get请求，data一般适用于post put 请求。
```
36. 请说下封装 vue 组件的过程？

```
答：

建立组件的模板，先把架子搭起来，写写样式，考虑好组件的基本逻辑。(os：思考1小时，码码10分钟，程序猿的准则。) 　　
准备好组件的数据输入。即分析好逻辑，定好 props 里面的数据、类型。　　
准备好组件的数据输出。即根据组件逻辑，做好要暴露出来的方法。　　
封装完毕了，直接调用即可
```
37. params和query的区别

```
答：用法：query要用path来引入，params要用name来引入，接收参数都是类似的，分别是 this.$route.query.name 和 this.$route.params.name 。url地址显示：query更加类似于我们ajax中get传参，params则类似于post，说的再简单一点，前者在浏览器地址栏中显示参数，后者则不显示

注意点：query刷新不会丢失query里面的数据 params刷新 会 丢失 params里面的数据。
```
38. vue初始化页面闪动问题

```
答：使用vue开发时，在vue初始化之前，由于 div 是不归 vue 管的，所以我们写的代码在还没有解析的情况下会容易出现花屏现象，看到类似于 {{message}} 的字样，虽然一般情况下这个时间很短暂，但是我们还是有必要让解决这个问题的。首先：在css里加上 [v-cloak] { display: none; } 。如果没有彻底解决问题，则在根元素加上 style="display: none;" :style="{display:  block }"
```
39. vue更新数组时触发视图更新的方法

```
答:push()；pop()；shift()；unshift()；splice()；sort()；reverse()
```
40. vue常用的UI组件库

```
答：Mint UI，element，VUX
```
41. vue修改打包后静态资源路径的修改

```
答：cli2 版本：将 config/index.js 里的 assetsPublicPath 的值改为  ./  。

build: { 
  // ... 
  assetsPublicPath:  ./ , 
  // ...  
} 
cli3版本：在根目录下新建vue.config.js 文件，然后加上以下内容：（如果已经有此文件就直接修改）

module.exports = { 
  publicPath: , // 相对于 HTML 页面（目录相同）    
} 

```

42. 什么是 vue 生命周期？有什么作用？

```
答：每个 Vue 实例在被创建时都要经过一系列的初始化过程——例如，需要设置数据监听、编译模板、将实例挂载到 DOM 并在数据变化时更新 DOM 等。同时在这个过程中也会运行一些叫做 生命周期钩子 的函数，这给了用户在不同阶段添加自己的代码的机会。（ps：生命周期钩子就是生命周期函数）例如，如果要通过某些插件操作DOM节点，如想在页面渲染完后弹出广告窗， 那我们最早可在mounted 中进行。
```
43. 第一次页面加载会触发哪几个钩子？

```
答：beforeCreate， created， beforeMount， mounted
```
44. 简述每个周期具体适合哪些场景

```
答：

beforeCreate： 在new一个vue实例后，只有一些默认的生命周期钩子和默认事件，其他的东西都还没创建。在beforeCreate生命周期执行的时候，data和methods中的数据都还没有初始化。不能在这个阶段使用data中的数据和methods中的方法

create： data 和 methods都已经被初始化好了，如果要调用 methods 中的方法，或者操作 data 中的数据，最早可以在这个阶段中操作

beforeMount： 执行到这个钩子的时候，在内存中已经编译好了模板了，但是还没有挂载到页面中，此时，页面还是旧的

mounted： 执行到这个钩子的时候，就表示Vue实例已经初始化完成了。此时组件脱离了创建阶段，进入到了运行阶段。如果我们想要通过插件操作页面上的DOM节点，最早可以在和这个阶段中进行

beforeUpdate： 当执行这个钩子时，页面中的显示的数据还是旧的，data中的数据是更新后的， 页面还没有和最新的数据保持同步

updated： 页面显示的数据和data中的数据已经保持同步了，都是最新的

beforeDestory： Vue实例从运行阶段进入到了销毁阶段，这个时候上所有的 data 和 methods ， 指令， 过滤器 ……都是处于可用状态。还没有真正被销毁

destroyed： 这个时候上所有的 data 和 methods ， 指令， 过滤器 ……都是处于不可用状态。组件已经被销毁了。
```
45. created和mounted的区别

```
答：created:在模板渲染成html前调用，即通常初始化某些属性值，然后再渲染成视图。mounted:在模板渲染成html后调用，通常是初始化页面完成后，再对html的dom节点进行一些需要的操作。
```
46. vue获取数据在哪个周期函数

```
答：一般 created/beforeMount/mounted 皆可. 比如如果你要操作 DOM , 那肯定 mounted 时候才能操作.
```
47. 请详细说下你对vue生命周期的理解？

```
答：总共分为8个阶段创建前/后，载入前/后，更新前/后，销毁前/后。

创建前/后： 在beforeCreated阶段，vue实例的挂载元素 $el 和 数据对象 data 都为undefined，还未初始化。在created阶段，vue实例的数据对象data有了， $el 还没有。

载入前/后： 在beforeMount阶段，vue实例的 $el 和data都初始化了，但还是挂载之前为虚拟的dom节点，data.message还未替换。在mounted阶段，vue实例挂载完成，data.message成功渲染。

更新前/后： 当data变化时，会触发beforeUpdate和updated方法。

销毁前/后： 在执行destroy方法后，对data的改变不会再触发周期函数，说明此时vue实例已经解除了事件监听以及和dom的绑定，但是dom结构依然存在。
```
48. mvvm 框架是什么？

```
答：vue是实现了双向数据绑定的mvvm框架，当视图改变更新模型层，当模型层改变更新视图层。在vue中，使用了双向绑定技术，就是View的变化能实时让Model发生变化，而Model的变化也能实时更新到View。
```
49. vue-router 是什么?它有哪些组件

```
答：vue用来写路由一个插件。router-link、router-view
```
50. active-class 是哪个组件的属性？

```
答：vue-router模块的router-link组件。children数组来定义子路由
```
51. 怎么定义 vue-router 的动态路由? 怎么获取传过来的值？

```
答：在router目录下的index.js文件中，对path属性加上/:id。使用router对象的params.id。
```
52. vue-router 有哪几种导航钩子?

```
答：三种，

第一种： 是全局导航钩子：router.beforeEach(to,from,next)，作用：跳转前进行判断拦截。

第二种： 组件内的钩子

第三种： 单独路由独享组件
```
53. `$route` 和 `$router` 的区别

```
答：$router 是VueRouter的实例，在script标签中想要导航到不同的URL,使用 $router.push 方法。返回上一个历史history用 $router.to(-1)

$route 为当前router跳转对象。里面可以获取当前路由的name,path,query,parmas等。
```
54. vue-router的两种模式

```
答: hash模式： 即地址栏 URL 中的 # 符号；

history模式： window.history对象打印出来可以看到里边提供的方法和记录长度。利用了 HTML5 History Interface 中新增的 pushState() 和 replaceState() 方法。（需要特定浏览器支持）。
```
55. vue-router实现路由懒加载（ 动态加载路由 ）

```
答:三种方式

第一种： vue异步组件技术 ==== 异步加载，vue-router配置路由 , 使用vue的异步组件技术 , 可以实现按需加载 .但是,这种情况下一个组件生成一个js文件。

第二种： 路由懒加载(使用import)。

第三种： webpack提供的require.ensure()，vue-router配置路由，使用webpack的require.ensure技术，也可以实现按需加载。这种情况下，多个路由指定相同的chunkName，会合并打包成一个js文件。
```
56. vuex是什么？怎么使用？哪种功能场景使用它？

```
答：vue框架中状态管理。在main.js引入store，注入。

新建了一个目录store.js，….. export 。

场景有：单页应用中，组件之间的状态。音乐播放、登录状态、加入购物车
```
57. vuex有哪几种属性？

```
答：有五种，分别是 State、 Getter、Mutation 、Action、 Module

state => 基本数据(数据源存放地)

getters => 从基本数据派生出来的数据

mutations => 提交更改数据的方法，同步！

actions => 像一个装饰器，包裹mutations，使之可以异步。

modules => 模块化Vuex
```
58. Vue.js中ajax请求代码应该写在组件的methods中还是vuex的actions中？

```
答：如果请求来的数据是不是要被其他组件公用，仅仅在请求的组件内使用，就不需要放入vuex 的state里。

如果被其他地方复用，这个很大几率上是需要的，如果需要，请将请求放入action里，方便复用。
```

## 六、Asp.Net Core

1. 如何在controller中注入service?

```
在config services方法中配置这个service。

在controller的构造函数中，添加这个依赖注入。
```
2. 谈一谈对DDD的理解?

```
DDD，领域驱动设计。就是通过领域来指导软件设计，是一种十分抽象的软件设计思想，它主要分为战略设计和战术设计

战略方面，通过事件风暴进行领域模型的划分，划分出核心域，子域，支撑域，定义通用语言，划分出界限上下文。

在战术设计方面，ddd将架构分层，“松耦合，高内聚”是架构设计的整体思想。按照DDD思想，可以分为领域层，基础设施层，应用层，接口层。

接口层为前端用户提供api接口。基础设施层可以放一些第三方的服务，数据库连接等内容。应用层是对领域服务的编排，是很薄的一层（目前我自己的架构，应用的是cqrs，所有的相关逻辑都是放在了应用层，而领域层只是放了实体，因为暂时还不是特别理解领域层的服务和事件都应该写什么）。领域层包括实体，值对象，聚合根，领域服务，领域事件等内容。
```
3. ASP.NET Core 比 ASP.NET 更具优势的地方是什么？

```
跨平台，ASP.NET Core 可以运行在 Windows 、Linux 和 MAC 系统上；

对框架本安装没有依赖，所有依赖都跟程序本身在一起；

ASP.NET Core 处理请求的效率更高，进而可以处理更多的请求；

ASP.NET Core 具有更多的安装配置方法。
```
4. asp.net core 主要的特性有哪些？

```
依赖注入。

日志系统架构。

引入了一个跨平台的网络服务器，kestrel。可以没有iis, apache和nginx就可以单独运行。

可以使用命令行创建应用。

使用AppSettings.json 来配置工程。

使用start up来注册服务。

更好的支持异步编程。

支持web socket和signal IR。

对于跨网站的请求的预防和保护机制。
```
5. ASP.NET Core Filter如何支持依赖注入?

```
可以通过全局注册，支持依赖注入

通过TypeFilter(typeof(Filter)) 标记在方法，标记在控制器

通过ServiceType(typeof(Filter))标记在方法，标记在控制器，必须要注册Filter这类；

TypeFilter和ServiceType的本质是实现了一个IFilterFactory接口；
```
6. Asp.Net Core中有哪些异常处理的方案？
```
1.继承Controller，重写OnActionExecuted

默认都会继承一个Controller类，重写OnActionExecuted，添加上异常处理即可。一般情况下我们会新建一个BaseController, 让所有Controller继承BaseController。代码如下

public class BaseController : Controller
{
    public override void OnActionExecuted(ActionExecutedContext context)
    {
        var exception = context.Exception;
        if (exception != null)
        {
            context.ExceptionHandled = true;
            context.Result = new ContentResult
            {
                Content = $"BaseController错误 : { exception.Message }"
            };
        }
        base.OnActionExecuted(context);
    }
}
2.使用 ActionFilterAttribute。

ActionFilterAttribute是一个特性，本身实现了 IActionFilter 及 IResultFilter , 所以不管是action里抛错，还是view里抛错，理论上都可以捕获。我们新建一个 ExceptionActionFilterAttribute, 重写 OnActionExecuted及OnResultExecuted，添加上异常处理，完整代码如下:

public class ExceptionActionFilterAttribute:ActionFilterAttribute
{
    public override void OnActionExecuted(ActionExecutedContext context)
    {
        var exception = context.Exception;
        if (exception != null)
        {
            context.ExceptionHandled = true;
            context.Result = new ContentResult
            {
                Content = $"错误 : { exception.Message }"
            };
        }
        base.OnActionExecuted(context);
    }

    public override void OnResultExecuted(ResultExecutedContext context)
    {
        var exception = context.Exception;
        if (exception != null)
        {
            context.ExceptionHandled = true;
            context.HttpContext.Response.WriteAsync($"错误 : {exception.Message}");
        }
        base.OnResultExecuted(context);
    }
}
使用方式有两种，
在controller里打上 [TypeFilter(typeof(ExceptionActionFilter)] 标签;

在Startup里以filter方式全局注入。

services.AddControllersWithViews(options =>
{
    options.Filters.Add<ExceptionActionFilterAttribute>();
})
3.使用 IExceptionFilter
我们知道, Asp.Net Core提供了5类filter, IExceptionFilter是其中之一，顾名思义，这就是用来处理异常的。Asp.net Core中ExceptionFilterAttribute已经实现了IExceptionFilter，所以我们只需继承ExceptionFilterAttribute，重写其中方法即可。 同样新建CustomExceptionFilterAttribute继承 ExceptionFilterAttribute，重写 OnException ，添加异常处理，完整代码如下：

public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
{
    public override void OnException(ExceptionContext context)
    {
        context.ExceptionHandled = true;
        context.HttpContext.Response.WriteAsync($"CustomExceptionFilterAttribute错误:{context.Exception.Message}");
        base.OnException(context);
    }
}
4.使用ExceptionHandler.

在 startup 里，vs新建的项目会默认加上.

if (env.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseExceptionHandler("/Home/Error");
}
5.自定义Middleare处理

通过middleware全局处理。

public class ErrorHandlingMiddleware
{
   private readonly RequestDelegate next;

   public ErrorHandlingMiddleware(RequestDelegate next)
   {
        this.next = next;
   }

   public async Task Invoke(HttpContext context)
   {
        try
        {
           await next(context);
        }
        catch (System.Exception ex)
        {
           //处理异常
        }
   }
}
```
7. 介绍ASP.NET Core中服务的生命周期？

```
ASP.NET Core支持依赖注入软件设计模式，它允许在不同的组件中注入我们的服务，并且控制服务的初始化。有些服务可以在短时间内初始化，并且只能在某个特别的组件，以及请求中才能用到；而还有一些服务，仅仅只用初始化一次，就可以在整个应用程序中使用。

Singleton --单例模式：

只有一个服务的实例被创建，这个实例，存储在内存中，可以在整个应用程序中使用。我们可以对一些初始化代价比较大的服务，使用Singleton模式。在代码中可以这样：

services.AddSingleton<IProductService, ProductService>();
Scoped --作用域

这种模式下，将会为每一个请求，都创建一个服务的实例。所有同一个请求中的中间件、MVC控制器，等等，都会得到一个相同的服务实例。Entity Framework Context就是一个Scoped服务最好的参考例子。我们可以通过使用AddScoped方法来使用Scoped模式：

services.AddScoped<IProductService, ProductService>();
Transient --短暂的、瞬变的　　
Transient模式下，服务每次被请求的时候，都会创建一个服务实例，这种模式特别适合轻量级、无状态的服务。我们可以使用AddTransient方法，来注入服务：

services.AddTransient<IProductService, ProductService>();
```
8. 什么是依赖注入？

```
依赖注入是一个过程，就是当一个类需要调用另一个类来完成某项任务的时候，在调用类里面不要去new被调用的类的对象，而是通过注入的方式来获取这样一个对象。具体的实现就是在调用类里面有一个被调用类的接口，然后通过调用接口的函数来完成任务。比如A调用B，而B实现了接口C，那么在A里面用C定义一个变量D，这个变量的实例不在A里面创建，而是通过A的上下文来获取。这样做的好处就是将类A和B分开了，他们之间靠接口C来联系，从而实现对接口编程。
```
9. 依赖注入有哪几种方式？

```
setter注入：

就是在类A里面定义一个C接口的属性D，在A的上下文通过B实例化一个对象，然后将这个对象赋值给属性D。主要就是set 与 get

构造函数注入：

就是在创建A的对象的时候，通过参数将B的对象传入到A中。
还有常用的注入方式就是工厂模式的应用了，这些都可以将B的实例化放到A外面，从而让A和B没有关系。还有一个接口注入，就是在客户类（A）的接口中有一个服务类(B)的属性。在实例化了这个接口的子类后，对这个属性赋值，这和setter注入一样。

接口注入

相比构造函数注入和属性注入，接口注入显得有些复杂，使用也不常见。具体思路是先定义一个接口，包含一个设置依赖的方法。然后依赖类，继承并实现这个接口。
```
10. 控制反转是什么？

```
控制反转（Inversion of Control，缩写为IoC），是面向对象编程中的一种设计原则，可以用来减低计算机代码之间的耦合度。其中最常见的方式叫做依赖注入（Dependency Injection，简称DI），还有一种方式叫“依赖查找”（Dependency Lookup）。

通过控制反转，对象在被创建的时候，由一个调控系统内所有对象的外界实体将其所依赖的对象的引用传递给它。也可以说，依赖被注入到对象中。
```
11. 依赖注入有哪些著名的框架？

```
Unity、autofac、http://spring.net、MEF、Injection、Asp.Net Core的ServiceCollection。
```
12. 介绍一些ABP.NEXT？

```
ASP.NET Boilerplate是.Net平台非常优秀的一个开源Web应用程序框架,在国内也有大量的粉丝.

从名称可以看出来, 这是ASP.NET Boilerplate的下一代框架。

ABP框架创作于2013年，那时候没有.Net Core和ASP.NET Core，也没有Angular2 +。ABP发布后，它们都是从头开发的。

ASP.NET Core引入了许多内置解决方案（扩展库），用于依赖注入，日志记录，缓存，本地化，配置等。它们实际上独立于ASP.NET Core，可用于任何类型的应用程序。
```
13. 什么是dot net core的startup class？

```
Startup class是dot net core应用的入口。所有的dot net core应用必须有这个class。这个类用来配置应用。这个类的调用是在program main函数里面进行配置的。类的名字可以自己定义。
```
14. startup class的configure方法有什么作用？

```
这个方法来定义整个应用如何响应HTTP请求。它有几个比较重要的参数，application builder，Hosting environment, logo factory， 在这里我们可以配置一些中间件用来处理路径，验证和session等等。
```
15. 什么是中间件（Middleware）？

```
中间件是组装到应用程序管道中以处理请求和响应的软件。 每个组件：

选择是否将请求传递给管道中的下一个组件。

可以在调用管道中的下一个组件之前和之后执行工作。

请求委托（Request delegates）用于构建请求管道，处理每个HTTP请求。

请求委托使用Run，Map和Use扩展方法进行配置。单独的请求委托可以以内联匿名方法（称为内联中间件）指定，或者可以在可重用的类中定义它。这些可重用的类和内联匿名方法是中间件或中间件组件。请求流程中的每个中间件组件都负责调用流水线中的下一个组件，如果适当，则负责链接短路。
```
16. 中间件的使用场景有哪些？

```
身份验证，Session存储，日志记录等。其实我们的Asp.net core项目中本身已经包含了很多个中间件。比如 身份认证中间件 UseAuthorization()等系列
```
17. 列举官方常用的中间件？

```
异常/错误处理： 
当应用在开发环境中运行时： 开发人员异常页中间件 (UseDeveloperExceptionPage) 报告应用运行时错误。 数据库错误页中间件报告数据库运行时错误； 
当应用在生产环境中运行时： 异常处理程序中间件 (UseExceptionHandler) 捕获以下中间件中引发的异常。 HTTP 严格传输安全协议 (HSTS) 中间件 (UseHsts) 添加 Strict-Transport-Security 标头。

HTTPS 重定向中间件 (UseHttpsRedirection) 将 HTTP 请求重定向到 HTTPS。

静态文件中间件 (UseStaticFiles) 返回静态文件，并简化进一步请求处理。

Cookie 策略中间件 (UseCookiePolicy) 使应用符合欧盟一般数据保护条例 (GDPR) 规定。

用于路由请求的路由中间件 (UseRouting)。

身份验证中间件 (UseAuthentication) 尝试对用户进行身份验证，然后才会允许用户访问安全资源。

用于授权用户访问安全资源的授权中间件 (UseAuthorization)。

会话中间件 (UseSession) 建立和维护会话状态。 如果应用使用会话状态，请在 Cookie 策略中间件之后和 MVC 中间件之前调用会话中间件。

用于将 Razor Pages 终结点添加到请求管道的终结点路由中间件（带有 MapRazorPages 的 UseEndpoints）。
```
18. 中间件的执行顺序？

```
异常/错误处理

HTTP 严格传输安全协议

HTTPS 重定向

静态文件服务器

Cookie 策略实施

身份验证

会话

MVC
```
19. application builder的use和run方法有什么区别？

```
这两个方法都在start up class的configure方法里面调用。都是用来向应用请求管道里面添加中间件的。Use方法可以调用下一个中间件的添加，而run不会。
```
20. dot net core 管道里面的map拓展有什么作用?

```
可以针对不同的路径添加不同的中间件。

 public void Configure(IApplicationBuilder app)
 {
   app.Map("/path1", Middleware1);
   app.Map("/path2", Middleware2);
 }

```
21. dot net core里面的路径是如何处理的？

```
路径处理是用来为进入的请求寻找处理函数的机制。所有的路径在函数运行开始时进行注册。

主要有两种路径处理方式， 常规路径处理和属性路径处理。常规路径处理就是用MapRoute的方式设定调用路径，属性路径处理是指在调用函数的上方设定一个路径属性。
```
22. dot net core工程里面有哪些常见的工程文件？

```
global, launch setting，app settings，bundle config，bower, package。
```
23. 依赖注入实现原理？

```
实现DI，核心在于依赖注入容器IContainer，该容器具有以下功能

①.（容器）保存可用服务的集合 // 要用的特定对象、特定类、接口服务

②.（注册）提供一种方式将各种部件与他们依赖的服务绑定到一起；// Add...函数或containerBuilder.Register函数，

③.（解析点）为应用程序提供一种方式来请求已配置的对象： 构造函数注入、属性注入.

运行时，框架会一层层通过反射构造实例，最终得到完整对象。
```
24. ASP.NET Core项目如何设置IP地址和端口号？

```
可以使用Properties文件夹下的launchSettings配置文件来配置不同的启动方式的时候，分别配置IP和端口号。
```

## 另外：问答型面试题：
 

1. C# 值类型和引用类型的区别

```
赋值时的区别：值类型的变量将直接获得一个真实的数据副本，初值为0；而对引用类型的赋值仅仅是把对象的引用赋给变量，这样就可能导致多个变量引用到一个实际对象实例上，初值为null。
内存分配的区别：值类型的对象会在堆栈上分配内存，而引用类型的对象将会在堆上分配内存。栈的空间相对有限，但运行效率却比堆高得多。
来自继承结构的区别：由于所有的值类型都有一个共同的基类：System.ValueType，所以值类型拥有一些引用类型不具有的共同性质。较为重要的一点是值类型的比较方法Equals 方法的实现有了改变。所有的值类型已经实现了内容的比较，而引用类型在没有重写Equals方法的情况下，仍然采用引用比较。
```

2. 面向对象的3个基本特征

```
封装：封装是面向对象的特征之一，是对象和类概念的主要特征。
封装详解：也就是把客观事物封装成抽象的类，并且类可以把自己的数据和方法只让可信的类或者对象操作，对不可信的进行信息隐藏。

继承：面向对象编程 (OOP) 语言的一个主要功能就是“继承”。继承是指这样一种能力：它可以使用现有类的所有功能，并在无需重新编写原来的类的情况下对这些功能进行扩展。
继承详解：通过继承创建的新类称为“子类”或“派生类”。被继承的类称为“基类”、“父类”或“超类”。继承的过程，就是从一般到特殊的过程。
要实现继承，可以通过“继承”（Inheritance）和“组合”（Composition）来实现。在某些 OOP 语言中，一个子类可以继承多个基类。但是一般情况下，一个子类只能有一个基类，要实现多重继承，可以通过多级继承来实现。

多态：多态性（polymorphisn）是允许你将父对象设置成为和一个或更多的他的子对象相等的技术，赋值之后，父对象就可以根据当前赋值给它的子对象的特性以不同的方式运作。简单的说，就是一句话：允许将子类类型的指针赋值给父类类型的指针。
实现多态，有二种方式，覆盖，重载：
1.覆盖，是指子类重新定义父类的虚函数的做法。
2.重载，是指允许存在多个同名函数，而这些函数的参数表不同（或许参数个数不同，或许参数类型不同，或许两者都不同）。

总结：我们知道，封装可以隐藏实现细节，使得代码模块化；继承可以扩展已存在的代码模块（类）；它们的目的都是为了——代码重用。而多态则是为了实现另一个目的——接口重用！多态的作用，就是为了类在继承和派生的时候，保证使用“家谱”中任一类的实例的某一属性时的正确调用。
```

3. string str=null与string str="" 有什么区别

```
string str=null 把这个引用指向了一个null，没有地址没有值的地方，即没分配内存空间
string str="" 把这个引用指向了一个地址，地址里面存的是空的字符，即占用了内存空间
```

4. StringBuilder有什么作用

```
简述：String 在进行运算时（如赋值、拼接等）会产生一个新的实例，而 StringBuilder 则不会。
所以在大量字符串拼接或频繁对某一字符串进行操作时最好使用 StringBuilder，不要使用 String
另外，对于 String 我们不得不多说几句：
1.它是引用类型，在堆上分配内存
2.运算时会产生一个新的实例
3.String 对象一旦生成不可改变（Immutable）
4.定义相等运算符（== 和 !=）是为了比较 String 对象（而不是引用）的值

总结：
StringBuilder采用构造器设计模式的思想高效地构造一个字符串对象，在构造过程中StringBuilder 可以有效的避免临时字符串对象的生成。一旦 StringBuilder的ToString方法被调用后，最终的字符串就被生成，而随后的操作将导致一个新的字符串对象的分配。因为字符串对象的不可修改特性，StringBuilder还经常被用来和非托管代码交互。
5.序列化有何作用
简述：通过流类型可以方便地操作各种字节流，但如何把现有的实例对象转换为方便传输的字节流，就需要用到序列化的技术。
```

6. 字符串池是如何提高系统性能的？

```
简述： 一旦使用了字符串池机制，当CLR启动的时候，会在内部创建一个容器，容器的键是字符串内容，而值是字符串在托管堆上的引用。当一个新的字符串对象需要分配时，CLR首先检测内部容器中是否已经包含了该字符对象，如果已经包含，则直接返回已经存在的字符串对象引用：如果不存在，则新分配一个字符串对象，同时把其添加到内部容器里去。但是当程序用new关键字显式地申明新分配的一个字符串对象时，该机制不会起作用。
```

7. 委托的原理

```
简述：委托是一类继承自 System.Delegate的类型, 每个委托对象至少包含了一个指向某个方法的指 针,该方法可以是实例方法,也可以是静态方法。委托实现了回调方法的机制,能够帮助程序员 设计更加简洁优美的面向对象程序。
```

8. 委托回调静态方法和实例方法有何区别

```
简述：委托回调静态方法和实例方法有何区别 当委托绑定静态方法时,内部的对象成员变量 _target将会被设置成 null,而当委托绑定实例 方法时, _target 将会设置成指向该实例方法所属类型的一个实例对象, 当委托被执行时, 该对象 实例将被用来调用实例方法。
```

9. 什么是链式委托？

```
简述：链式委托是指一个委托的链表,而不是指另一类特殊的委托。 当执行链上的一个方法时,后续委托方法将会被依次执行。System.Multicast Delegate定 义了对链式委托的支持。在System.Delegate的基础上,它增加了一个指向后续委托的指针,这样就实现了一个简单的链表结构。
```